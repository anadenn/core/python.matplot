import matplotlib.pyplot as plt
import numpy as np
from mod_program import Object
from dataModel import *

class Figure_Node(Object):

    def make(self,fig,ax):
        self.onMake(fig,ax)
        for elt in self.children.by_class(Figure_Node):
            elt.make(self.fig, self.ax)

    def onMake(self,fig,ax):
        pass


class Figure(Object):

    TITLE=String()

    def make(self):
        fig, ax = plt.subplots()
        for elt in self.children.by_class(Figure_Node):
            elt.make(fig, ax)
        ax.set_title(self.title)
        ax.legend() 
        plt.show()

class Axis(Figure_Node):

    LABEL=String()

    def onMake(self,fig,ax):
        if self.name=="x":
            ax.set_xlabel(self.label)
        elif self.name=="y":
            ax.set_ylabel(self.label)
        


class Plot(Figure_Node):

    LABEL=String()

    def onMake(self,fig,ax):
        x,y=self.getData()
        ax.plot(x, y, label=self.label)

    def getData(self):
        x = np.linspace(0, 1, 100)
        y=np.random.rand(100)
        return x,y

