from dataModel.models import *
from core.program import *
from dataModel.Store import Store



#=================================================================

class Axe(Object):

#=================================================================

    CONTENT=List()
    TEXT=String()
    DEFAULT=String()

    FLOAT=Boolean(default=False)
    TIME=Boolean(default=False)


    #------------------------------------------------------------
    def process(self,node):

        #try:
         
       self.content.append(self.onProcessString(node))
        #except:
        #    self.content.append(self.default)

    #------------------------------------------------------------
    def onProcessString(self,node):
        return eval(self.text%node)


    #------------------------------------------------------------
    def get_intervals(self):
        """
        appeler la fonction pour generer la liste
        si deja faite, renvoie la liste
        """
        if hasattr(self,"intervals") !=True:

            self.make_intervals()
        return self.intervals

    #------------------------------------------------------------
    def make_intervals(self):

        self.intervals=list()
        dt0=self.content[0]
        for elt in self.content[1:]:

            self.intervals.append(elt-dt0)
            dt0=elt

    #------------------------------------------------------------
    def clear(self):
        del self.content
        self.content=list()

    #------------------------------------------------------------
#=================================================================

class AxeTime(Axe):

#=================================================================

    CONVERSION=String(default="%Y-%m-%d %H:%M:%S")

    #------------------------------------------------------------
    def onProcessString(self,node):
        string=Axe.onProcessString(self,node)
        elt= time.strptime(string, self.conversion)
        return time.strftime('%s',elt)
#------------------------------------------------------------
#=================================================================

class AxeFloat(Axe):

#=================================================================

    #------------------------------------------------------------
    def onProcessString(self,node):
        return float(Axe.onProcessString(self,node))

#------------------------------------------------------------
#=================================================================

class AxeInt(Axe):

#=================================================================

    #------------------------------------------------------------
    def onProcessString(self,node):
        return int(Axe.onProcessString(self,node))

#------------------------------------------------------------


#=================================================================
