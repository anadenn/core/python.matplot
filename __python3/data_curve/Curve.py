from dataModel.models import *
from core.program import *
from dataModel.Store import Store
from .Graphic import Graphic

#==========================================================

class Curve(Graphic):
    
#==========================================================
    """

    """
    GET_NODES=Function(ReturnMerge())
    #------------------------------------------------------------
    def onMake(self):
        yield "CURVE "+self.path()
        for node in self.get_nodes():
            self.onAddRecord(node)

    #------------------------------------------------------------
    def onClear(self):
        self.get_axes().execute("clear")

    #------------------------------------------------------------
    def onInfos(self):
        return [elt.content for elt in self.get_axes()]


    #------------------------------------------------------------
    def get_nodes(self):
        for elt in self.children.by_class(Select):
            yield elt.get_selection()

    #------------------------------------------------------------
    def onAddRecord(self,node):
        for axe in self.get_axes():
            axe.process(node)

   #------------------------------------------------------------



#==========================================================
